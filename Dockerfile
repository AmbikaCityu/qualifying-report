FROM ubuntu:rolling

# Enviorment variables
ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Asia/Hong_Kong
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# Install dependencies
RUN apt-get update \
    && apt-get install -y \
    fonts-stix \
    make \
    inkscape \
    r-base \
    r-cran-tidyverse \
    r-cran-rmarkdown \
    r-cran-bookdown \
    pandoc \
    pandoc-citeproc \
    && rm -rf /var/lib/apt/lists/*

CMD /bin/bash
