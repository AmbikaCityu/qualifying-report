.PHONY: _book references-from-zotero.bib 3d img final

all: html html_book

html: report.html

html_booK: _book

pdf: report.pdf

final: silva-raniere.pdf

silva-raniere.pdf: cover.pdf report.pdf
	pdftk $^ output $@

cover.pdf: cover.odt
	libreoffice --convert-to pdf $<

_book: references-from-zotero.bib 3d img
	Rscript -e 'bookdown::render_book(".", "bookdown::gitbook")'

report.html: references-from-zotero.bib 3d img
	Rscript -e 'bookdown::render_book(".", "bookdown::html_document2")'

report.tex: report.html
	pandoc \
		$^ \
		--to latex \
		--from html \
		--extract-media media \
		--output $@ \
		--standalone \
		--bibliography references-from-zotero.bib
	sed -i 's/❌/x/g' report.tex

report.pdf: report.tex
	pdflatex $<

3d:
	$(MAKE) -C $@

img:
	$(MAKE) -C $@

references-from-zotero.bib:
	@echo 'Use Better BibTeX for Zotero <https://retorque.re/zotero-better-bibtex> to keep keys updated.'
