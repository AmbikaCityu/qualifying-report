# Qualifying Report

Preview: https://raniere-phd.gitlab.io/qualifying-report/report.html

## Guidebookfor Research Degree Studies 2020/21

1. Students  must  submit  a  qualifying  report  (typed  and  in English)  within  the  specified qualifying period as follows:

   - Full-time: Within 6–12 months from start of study
   - Part-time: Within 9–18 months from start of study
2. A qualifying report should include a survey of the relevant literature, an identification of a specific  research topic,  the  research  methodology  and  a  discussion  on  possible outcomes.
3. Students should submit three copies of the qualifying report (with Form SGS35) to their research  supervisors within  the  specified  qualifying  period.  The Qualifying Panel  will assess the student’s suitability to continue his or her studies on the basis of the qualifying report, coursework results and any other assessment as considered appropriate by the Panel. The Panel’s recommendations will be forwarded to the School/Department for approval.
